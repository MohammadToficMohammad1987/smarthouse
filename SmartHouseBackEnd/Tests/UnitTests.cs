﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using SmartHouseBackEnd.Data;
using SmartHouseBackEnd.Models;
using SmartHouseBackEnd.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace SmartHouseBackEnd.Tests
{
    public  class UnitTests
    {
        private readonly ISmartHouseService _smartHouseService;
        private readonly UserManager<SmartHouseUser> _userManager;
        private string testUserId;
        private readonly ILogger<UnitTests> _logger;

        public  UnitTests(ISmartHouseService smartHouseService,
                         UserManager<SmartHouseUser> userManager,
                         ILogger<UnitTests> logger)
        {

            _smartHouseService = smartHouseService;
            _userManager = userManager;
            _logger = logger;
            testUserId = "";


        }


        public void UnitTest() {

            System.Diagnostics.Debug.WriteLine("UnitTest Start*****************************************");
            System.Diagnostics.Debug.WriteLine(testUserId);

            CreateTestUser();
            IsTestLightOFF();
            IsTestLightOnAfterSwitchOn();
            IsTestLightOFFAfterSwitchOFF();
            DeleteTestUser();

            System.Diagnostics.Debug.WriteLine("UnitTest End *****************************************");
        }

        public  void IsTestLightOFF()
        {
            var result = _smartHouseService.GetContextByUserId(testUserId).Result.light;
            Assert.True((result == LightType.OFF), "Light Not Off");
        }


       
        public void IsTestLightOnAfterSwitchOn()
        {
            Context context = _smartHouseService.GetContextByUserId(testUserId).Result;
            context.light = LightType.ON;
            _ = _smartHouseService.UpdateContext(context).Result;


            var result = _smartHouseService.GetContextByUserId(testUserId).Result.light;
            Assert.True((result == LightType.ON), "Light Should be ON");
        }



        public void IsTestLightOFFAfterSwitchOFF()
        {
            Context context = _smartHouseService.GetContextByUserId(testUserId).Result;
            context.light = LightType.OFF;
            _ = _smartHouseService.UpdateContext(context).Result;


            var result = _smartHouseService.GetContextByUserId(testUserId).Result.light;
            Assert.True((result == LightType.OFF), "Light Should be OFF");
        }







        public void CreateTestUser() {

            var user =  _userManager.FindByNameAsync(Constants.testUserName).Result;

            if (user != null)
            {
                testUserId = user.Id;
                DeleteTestUser();
            }

                user = new SmartHouseUser
                {
                    UserName = Constants.testUserName,
                    Email = Constants.testUserName,
                    EmailConfirmed = true,
                    PhoneNumber = "8888888"
                };

                SmartHouse smartHouse = new SmartHouse();
                user.smartHouse = smartHouse;
                var result = _userManager.CreateAsync(user, Constants.password).Result;
                Assert.True((result == IdentityResult.Success), "TestUser Creation Failed");
                testUserId = user.Id;
            
        }


        public void DeleteTestUser()
        {
            var user = _userManager.FindByIdAsync(testUserId).Result;
            Assert.True((user != null), "TestUser deletion Failed");
            var result = _userManager.DeleteAsync(user).Result;
            Assert.True((result == IdentityResult.Success), "TestUser Deletion Failed");
        }


    }
}
