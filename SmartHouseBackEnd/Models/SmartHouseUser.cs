﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartHouseBackEnd.Models
{
    public class SmartHouseUser : IdentityUser
    {
        public SmartHouse smartHouse { get; set; }
    }
}
