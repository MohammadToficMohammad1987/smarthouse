﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartHouseBackEnd.Models
{
    public class Context
    {
        public LightType light { get; set; } = LightType.OFF;

        public int temperature { get; set; } = 18;

        public string userName { get; set; } = "Name";

        public string userId { get; set; } = "";
    }
}
