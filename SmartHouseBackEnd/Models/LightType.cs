﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartHouseBackEnd.Models
{
    public enum LightType
    {
        OFF =0,
        DIMMED=1,
        ON=2
    }
}
