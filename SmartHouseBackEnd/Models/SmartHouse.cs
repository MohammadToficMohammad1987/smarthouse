﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartHouseBackEnd.Models
{
    public class SmartHouse
    {
        public int Id { get; set; }

        public LightType light { get; set; } = LightType.OFF;

        public int temperature { get; set; } = 18;

        public String userId { get; set; }
        public SmartHouseUser user { get; set; }
    }
}
