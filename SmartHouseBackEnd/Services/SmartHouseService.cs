﻿using Microsoft.AspNetCore.Identity;
using SmartHouseBackEnd.Data;
using SmartHouseBackEnd.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace SmartHouseBackEnd.Services
{
    public class SmartHouseService : ISmartHouseService
    {
        private readonly UserManager<SmartHouseUser> _userManager;
        private readonly ApplicationDBContext _applicationDBContext;

        public SmartHouseService(UserManager<SmartHouseUser> userManager,
                                 ApplicationDBContext applicationDBContext) {
            _userManager = userManager;
            _applicationDBContext = applicationDBContext;
        }
        public async Task<Context> GetContextByUserId(string userId)
        {
            var user = await _applicationDBContext.SmartHouseUsers.Include(users => users.smartHouse).Where(user => user.Id.Equals(userId)).FirstOrDefaultAsync();
            Context context = new Context();
            context.userName = user.UserName;
            context.userId = user.Id;
            context.light = user.smartHouse.light;
            context.temperature = user.smartHouse.temperature;
            return context;
        }

       

        public async Task<int> UpdateContext(Context context)
        {
            var user = await _applicationDBContext.SmartHouseUsers.Include(users => users.smartHouse).Where(user => user.Id.Equals(context.userId)).FirstOrDefaultAsync();
            user.smartHouse.light = context.light;
            user.smartHouse.temperature = context.temperature;
            return await _applicationDBContext.SaveChangesAsync();


        }

       
    }
}
