﻿using Microsoft.AspNetCore.Identity;
using SmartHouseBackEnd.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartHouseBackEnd.Services
{
    public interface ISmartHouseService
    {
       

        Task<Context> GetContextByUserId(String userId);
        Task<int> UpdateContext(Context context);
    }
}
