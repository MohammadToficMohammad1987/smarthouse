﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SmartHouseBackEnd.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartHouseBackEnd.Data
{
    public class ApplicationDBContext : IdentityDbContext<SmartHouseUser>
    {
        public ApplicationDBContext(DbContextOptions<ApplicationDBContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);

            builder.Entity<SmartHouseUser>().HasOne(u => u.smartHouse).WithOne(s => s.user).HasForeignKey<SmartHouse>(x => x.userId).OnDelete(DeleteBehavior.Cascade);
           
         }
        public DbSet<SmartHouseUser> SmartHouseUsers { get; set; }
    }
}
