﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SmartHouseBackEnd.Models;
using SmartHouseBackEnd.Services;

namespace SmartHouseBackEnd.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class RestController : ControllerBase
    {

        private readonly ISmartHouseService _smartHouseService;
        private readonly UserManager<SmartHouseUser> _userManager;

        public RestController(ISmartHouseService smartHouseService,
                              UserManager<SmartHouseUser> userManager) {

            _smartHouseService = smartHouseService;
            _userManager = userManager;
        }


       
        [HttpGet]
        public async Task<ActionResult<Context>> GetContext()
        {
            try
            {
                var user = await _userManager.GetUserAsync(User);
                Context context = await _smartHouseService.GetContextByUserId(user.Id);
                return context;
            }
            catch (Exception e) {
                return NotFound();
            }
        }

       
        [HttpPost]
        public async Task<ActionResult<int>> UpdateContext([FromBody] Context context)
        {
            try
            {
               
                return await _smartHouseService.UpdateContext(context);
            }
            catch (Exception e) {
                return NoContent();
            }

        }


        //stateless Api
        [HttpGet("{userId}")]
        public async Task<ActionResult<Context>> GetContextById(string userId)
        {
            try
            {
                Context context = await _smartHouseService.GetContextByUserId(userId);
                return  context;
            }
            catch (Exception e) {
                return NotFound();
            }
        }

    }
}